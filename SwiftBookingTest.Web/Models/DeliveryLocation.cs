﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Models
{
    public class DeliveryLocation
    {
        public int ID { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(20)]
        [DisplayFormat(NullDisplayText = "")]
        public string Phone { get; set; }
        
        public bool Submitted { get; set; }

        [Display(Name = "Response ID")]
        [DisplayFormat(NullDisplayText = "")] 
        public string ResponseID { get; set; }

        [Display(Name = "API Response")]
        [DisplayFormat(NullDisplayText = "")]
        public string LastResponse { get; set; }
    }
}