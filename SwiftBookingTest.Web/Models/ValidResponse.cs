﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Models
{
    public class DeliveryBookingResponseModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Quote quote { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeliveryDetailsModel delivery { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeliveryBookingModel request { get; set; }
    }
    
    public class Quote 
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime created { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime start { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double distanceKm { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PriceModel fee { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public QuoteLocationDetail pickup { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public QuoteLocationDetail dropoff { get; set; }
    }
    public class PriceModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double cost { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int costCents { get; set; }
    }
    public class QuoteLocationDetail {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeEstimateModel time { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address { get; set; }
    }
    public class TimeEstimateModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime average { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime earliest { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime latest { get; set; }
    }
    public class DeliveryDetailsModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime created { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string reference { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationAPIModel pickupLocation { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationAPIModel dropoffLocation { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime lastUpdated { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string currentStatus { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DriverModel driver { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string[] items { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime pickupTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeFrameModel dropoffTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string deliveryInstructions { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string customerReference { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TrackingUrlsModel trackingUrls { get; set; }
    }
    public class LocationAPIModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string phone { get; set; }
    }
    public class DriverModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string identifier { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string phone { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string photoUrl { get; set; }
    }
    public class TimeFrameModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime earliestTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime latestTime { get; set; }
    }
    public class TrackingUrlsModel {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string www { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string api { get; set; }
    }
     public class DeliveryBookingModel 
     {
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string reference { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string deliveryInstructions { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool itemsRequirePurchase { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeliveryBookingItemModel[] items { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime pickupTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public  DeliveryBookingLocationModel pickupDetail { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeFrameModel dropoffWindow { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeliveryBookingLocationModel dropoffDetail { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double customerFee { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string customerReference { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double tax { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool taxInclusivePrice { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double tip { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double driverFeePercentage { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string driverMatchCode { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int deliverySequence { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DeliveryEventWebhookModel[] webhooks { get; set; }
}
     public class DeliveryBookingItemModel {
         public int quantity { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string sku { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string description { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double price { get; set; }
        }
     public class DeliveryBookingLocationModel {
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string phone { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string email { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string description { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ExtraAddressDetails additionalAddressDetails { get; set; }
        }
     public class ExtraAddressDetails {
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string stateProvince { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string country { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string suburbLocality { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string postcode { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double latitude { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double longitude { get; set; }
        }
     public class DeliveryEventWebhookModel {
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string eventName { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string url { get; set; }
       }   
}