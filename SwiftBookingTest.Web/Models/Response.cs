﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Models
{
    public class Response
    {
        public string Code { get; set; }
        public string Message { get; set; }

    }
}