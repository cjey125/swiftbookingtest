﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Models
{
    public class DeliveryLocationsViewModel
    {
        public List<DeliveryLocation> DeliveryLocations { get; set; }
        public DeliveryLocation SelectedDeliveryLocation { get; set; }
        public string DisplayMode { get; set; }
        public string ResultMessages { get; set; }
    }
}