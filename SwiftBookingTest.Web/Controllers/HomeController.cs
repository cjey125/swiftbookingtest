﻿using SwiftBookingTest.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SwiftBookingTest.Web.Controllers
{
    public class HomeController : Controller
    {
        private DeliveryLocationContext db = new DeliveryLocationContext();

        public ActionResult Index(string txtName, string txtPhone, string txtAddress)
        {
            if (!(String.IsNullOrEmpty(txtName) && String.IsNullOrEmpty(txtPhone) && String.IsNullOrEmpty(txtAddress)
                ))
            {
                try
                {
                    var location = new DeliveryLocation { Name = txtName, Address = txtAddress, Phone = txtPhone };
                    db.DeliveryLocations.Add(location);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    // When not using ModelView that contains both the list and new item, validation attribute might cause exception
                    TempData["Message"] = "Error Creating Location: " + ex.Message;
                }
            }

            return View(db.DeliveryLocations.ToList());
        }

        public ActionResult Details(int id = 0)
        {
            DeliveryLocation deliverylocation = db.DeliveryLocations.Find(id);
            if (deliverylocation == null)
            {
                return HttpNotFound();
            }
            return View(deliverylocation);
        }


        //
        // GET: /DeliveryLocations/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DeliveryLocation deliverylocation = db.DeliveryLocations.Find(id);
            if (deliverylocation == null)
            {
                return HttpNotFound();
            }
            return View(deliverylocation);
        }

        //
        // POST: /DeliveryLocations/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DeliveryLocation deliverylocation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliverylocation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(deliverylocation);
        }

        //
        // GET: /DeliveryLocations/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DeliveryLocation deliverylocation = db.DeliveryLocations.Find(id);
            if (deliverylocation == null)
            {
                return HttpNotFound();
            }
            return View(deliverylocation);
        }

        //
        // POST: /DeliveryLocations/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DeliveryLocation deliverylocation = db.DeliveryLocations.Find(id);
            db.DeliveryLocations.Remove(deliverylocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult ClearAll()
        {
            foreach (var dl in db.DeliveryLocations)
            {
                db.DeliveryLocations.Remove(dl);
            }
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        public ActionResult SendToGetSwift(int id)
        {
            SendToGetSwiftAsync(id).Wait();
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        public ActionResult SendAllToGetSwift()
        {
            foreach (var dl in db.DeliveryLocations)
            {
                SendToGetSwiftAsync(dl.ID).Wait();

            }
            db.SaveChanges();
            return RedirectToAction("Index");

        }


        public async Task SendToGetSwiftAsync(int id)
        {

            using (var handler = new HttpClientHandler { Credentials = CredentialCache.DefaultNetworkCredentials })
            {
                using (var client = new HttpClient(handler))
                {
                    DeliveryLocation deliverylocation = db.DeliveryLocations.Find(id);
                    if (deliverylocation == null)
                        return;
                    var uri = new Uri(ConfigurationManager.AppSettings["GetSwiftDeliveriesUrl"]);

                    var postData = new
                    {
                        ApiKey = ConfigurationManager.AppSettings["ApiKey"],
                        Booking = new
                        {
                            Reference = id.ToString(),
                            PickupDetail = new
                            {
                                Name = "Warehouse",
                                Address = "9 Ashley st, Footscray, VIC"
                            },
                            DropoffDetail = new
                            {
                                Name = deliverylocation.Name,
                                Address = deliverylocation.Address,
                                Phone = deliverylocation.Phone
                            }
                        }
                    };
                    HttpResponseMessage response = await client.PostAsJsonAsync<object>(uri.ToString(), postData).ConfigureAwait(continueOnCapturedContext: false);


                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsAsync<DeliveryBookingResponseModel>().Result;
                        deliverylocation.Submitted = true;
                        deliverylocation.ResponseID = result.delivery.id;
                        deliverylocation.LastResponse = result.delivery.trackingUrls.api;
                    }
                    else
                    {
                        var result = response.Content.ReadAsAsync<Response>().Result;
                        deliverylocation.LastResponse = "Error in posting request:" + result.Message;
                        TempData["ResultMessage"] = String.IsNullOrEmpty((String)TempData["ResultMessage"]) ? result.Message : TempData["ResultMessage"] + "\r\n" + result.Message;
                    }
                    db.Entry(deliverylocation).State = EntityState.Modified;
                }
            }
        }

    }
}
